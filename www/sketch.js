
// Loaded images (res folder)
let carImage;
let goalImage;

// HTML element tags
let containerElement = "sketch-container";
let resetPopulationButtonElement = "reset-population-button";
let resetSimulationButtonElement = "reset-simulation-button";
let simulationSpeedSliderElement = "slider-speed-range";
let simulationSpeedLabelElement = "slider-speed-label";
let simulationCarSizeSliderElement = "slider-car-size-range";
let simulationCarSizeLabelElement = "slider-car-size-label";
let simulationLengthSliderElement = "slider-simulation-length-range";
let simulationLengthLabelElement = "slider-simulation-length-label";
let simulationMutateChanceSliderElement = "slider-simulation-mutability-range";
let simulationMutateChanceLabelElement = "slider-simulation-mutability-label";

let goal; // Needed to reset Simulation

let simulationSteps;
let calculationsPerFrame;
let carSize;
let mutateChance;

function preload() {
    carImage = loadImage("res/car.png");
    goalImage = loadImage("res/goal.png");

    // Connect Reset Population Button to function
    // Restarts simulation with the same obstacles
    let resetPopButton = document.getElementById(resetPopulationButtonElement);
    resetPopButton.onclick = function() {
	let obstacles = simulation.obstacles.slice();
        simulation = new Simulation(simulatedCarsNum, 500, 500, goal, simulationSteps, carSize, mutateChance);
	simulation.obstacles = obstacles;
    }

    let resetSimButton = document.getElementById(resetSimulationButtonElement);
    resetSimButton.onclick = function() {
        simulation = new Simulation(simulatedCarsNum, 500, 500, goal, simulationSteps, carSize, mutateChance);
    }

    let simSpeedSlider = document.getElementById(simulationSpeedSliderElement);
    calculationsPerFrame = simSpeedSlider.value;
    simSpeedSlider.oninput = function() {
	let speedLabel = document.getElementById(simulationSpeedLabelElement);
	speedLabel.innerHTML = "Speed: " + this.value;
	calculationsPerFrame = this.value;
    }

    let carSizeSlider = document.getElementById(simulationCarSizeSliderElement);
    carSize = Number(carSizeSlider.value);
    carSizeSlider.oninput = function() {
	let sizeLabel = document.getElementById(simulationCarSizeLabelElement);
	sizeLabel.innerHTML = "Car Size: " + this.value;
        carSize = Number(this.value);
    }

    let simLengthSlider = document.getElementById(simulationLengthSliderElement);
    simulationSteps = Number(simLengthSlider.value) * 60;
    simLengthSlider.oninput = function() {
	let lenLabel = document.getElementById(simulationLengthLabelElement);
	lenLabel.innerHTML = "Simulation Length [s]: " + this.value;
        simulationSteps = Number(this.value) * 60;
    }

    let simMutateChanceSlider = document.getElementById(simulationMutateChanceSliderElement);
    mutateChance = Number(simMutateChanceSlider.value);
    simMutateChanceSlider.oninput = function() {
	let mutLabel = document.getElementById(simulationMutateChanceLabelElement);
	mutLabel.innerHTML = "Mutate Chance: " + this.value;
        mutateChance = Number(this.value);
    }

}

// Holds mouse locations upon click and unclick
var startClickLoc;
var endClickLoc;

// Simulation related values
let simulation; // Holds base Simulation object
let simulatedCarsNum = 100;

function setup()
{
    // TODO: reimplement goal
    // Simulation setup
    goal = new Goal(windowWidth / 2, 50, 50);

    simulation = new Simulation(simulatedCarsNum, 500, 500, goal, simulationSteps, carSize, mutateChance);
    simulation.update();
    simulation.show();

    // Canvas setup
    var canvasDiv = document.getElementById(containerElement);
    var canvas = createCanvas(canvasDiv.offsetWidth, canvasDiv.offsetHeight);
    canvas.parent(containerElement) // HTML Div element
    resizeCanvas(canvasDiv.offsetWidth, canvasDiv.offsetHeight); // Ensures correct size
    background(95);
    colorMode(HSB);
    noStroke();
    startClickLoc = createVector();
    endClickLoc = createVector();
}

// Shows the obstacle being currently drawn
function drawObstacleEditRect() {
    push();
    let obst = new Obstacle(startClickLoc.x, startClickLoc.y, mouseX - startClickLoc.x, mouseY - startClickLoc.y);
    obst.show();
    pop();
}

// Main draw loop. Called 60 times per second
function draw()
{
    background(95);

    if(mouseIsPressed) { // Drawing obstacles, do not keep updating
	simulation.show();
	drawObstacleEditRect();
        return; // Do not update Simulation
    }

    for(let i = 0; i < calculationsPerFrame; i++) {
	simulation.update();
    }
    simulation.show();
}

// Returns an Obstacle with the bounds of the click and release
function getClickBoundsObstacle() {
    rectStartX = Math.min(startClickLoc.x, endClickLoc.x);
    rectStartY = Math.min(startClickLoc.y, endClickLoc.y);
    rectEndX = Math.max(startClickLoc.x, endClickLoc.x);
    rectEndY = Math.max(startClickLoc.y, endClickLoc.y);
    rectWidth = rectEndX - rectStartX;
    rectHeight = rectEndY - rectStartY;
    return new Obstacle(rectStartX, rectStartY, rectWidth, rectHeight);
}

function mousePressed() {
    startClickLoc = createVector(mouseX, mouseY);

    //return false; // Disables default browser mousePressed functionality
}

function mouseReleased() {
    endClickLoc = createVector(mouseX, mouseY);
    simulation.addObstacle(getClickBoundsObstacle());

    //return false; // Disables default browser mouseReleased functionality
}

function keyTyped() {
    if(key === "d") {
	let vec = createVector(mouseX, mouseY);
	simulation.deleteObstacleAt(vec);
    }

    return false; // Disable default behavior

}

// Ensures window stays the correct size
function windowResized() {
  var canvasDiv = document.getElementById(containerElement);
  resizeCanvas(canvasDiv.offsetWidth, canvasDiv.offsetHeight);
}
