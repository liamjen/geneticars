// Holds and maintains list of Gene
class DNA
{
    // length: number of genes
    constructor(length, mutateChance)
    {
        this.dna = [];
        this.length = length;
	this.mutateChance = mutateChance;

	// Seed with completely random genes
        for(var i = 0; i < length; i++)
        {
            this.dna[i] = new Gene(randomWheelState(), randomAcceleratorState());
        }
    }

    // Takes another DNA, and has a 50% chance to
    // share each gene
    splice(otherDNA)
    {
        for(var i = 0; i < this.length; i++)
        {
	    // 50% chance to switch genes
            if(Math.random() > 0.5)
            {
                var gene = this.dna[i];
                this.dna[i] = otherDNA.dna[i];
                otherDNA.dna[i] = gene;
            }
        }
    }

    // Mutates each gene to a random gene based on
    // mutateChance
    mutate()
    {
        for(var i = 0; i < this.length; i++)
        {
            if(Math.random() < this.mutateChance)
            {
                this.dna[i] = new Gene(randomWheelState(), randomAcceleratorState());
            }
        }
    }
}
