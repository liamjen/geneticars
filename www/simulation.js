//TODO: Max frame length should exist here
//
class Simulation
{
    // carNum: number of cars in the simulation (population)
    // startX: starting X of all cars
    // startY: starting Y of all cars
    constructor(carNum, startX, startY, goal, lifetimeSteps, carSize, mutateChance)
    {
	this.population = new Population(carNum, startX, startY, lifetimeSteps, carSize, mutateChance);
	this.carNum = carNum;
	this.carSize = carSize;
	this.startX = startX;
	this.startY = startY; // Keep as members for resetting Population
	this.startPos = createVector(this.startX, this.startY);
	this.goal = goal;
	this.obstacles = [];
	this.lifetimeSteps = lifetimeSteps;
	this.currentStep = 0;
	this.currentGeneration = 1;
	this.topScore = 0; // Top score from all generations
	this.topGenerationScore = 0; // Top score after last generation
    }

    addObstacle(obstacle) {
        this.obstacles.push(obstacle);
    }

    // Calculates if cars ran into an obstacle, or if the car
    // found the goal (both mark car as dead)
    findAllCarCollisions() {
	for(var i = 0; i < this.population.cars.length; i++) {
	    var car = this.population.cars[i];
	    
	    if(this.goal.collidesWith(car)) {
		car.dead = true;
                car.foundGoal = true;
	    }

	    for(var j = 0; j < this.obstacles.length; j++) {
		var obstacle = this.obstacles[j];
		if(obstacle.collidesWith(car)) {
		    car.dead = true;
		}
	    }
	}
    }

    // Assigns the score to the car
    setScore(car) {
	if(car.foundGoal) {
	    car.score = this.lifetimeSteps * 3; // Awards most points
	    car.score += (1 / Math.max(1, car.deadStep)) * this.lifetimeSteps; // Faster cars get more points
	} else { // Did not find goal
	    // Start score as the number of frames the car stayed alive for
	    if(car.dead) {
		car.score = car.deadStep; // Car score starts as how many steps it could stay alive for.
	    } else {
		car.score = this.lifetimeSteps; // Car lived the whole time
	    }

	    let goalDistScore = 2 * (this.lifetimeSteps * (1 / Math.max(1, this.goal.pos.dist(car.pos))));
	    let carDistScore = Math.min(goalDistScore, car.odometer);
	    carDistScore = Math.min(goalDistScore, carDistScore);

	    car.score += goalDistScore; // Closer to goal = better score
	    car.score += carDistScore; // Moving further means higher score as well TODO: weighted too much
	}
	// End calculating score

	// Update top score variables (to display)
	if(car.score > this.topScore) {
	    this.topScore = car.score;
	}

	if(car.score > this.topGenerationScore) {
	    this.topGenerationScore = car.score;
	}
    }

    deleteObstacleAt(vector) {
	for(let i = 0; i < this.obstacles.length; i++) {
	    let obstacle = this.obstacles[i];
	    if(obstacle.collidesWithPoint(vector)) {
		this.obstacles.splice(i, 1);
		i--;
	    }
	}
    }

    // Advances the simulation by 1 step (generally called each frame)
    update()
    {
	if (this.currentStep >= this.lifetimeSteps || this.population.allDead) {
	    this.topGenerationScore = 0;
	    // Find top performing cars
	    // population.populate(cars)
    
	    // Find top performing cars
	    // Use functinal list sort?
	    let car1 = this.population.cars[0];
	    let car2 = this.population.cars[1];
	    this.setScore(car1);
	    this.setScore(car2);

	    for(var i = 2; i < this.population.cars.length; i++) {
		var currentCar = this.population.cars[i];
		this.setScore(currentCar);
		if(currentCar.score > car1.score) {
		     car1 = currentCar;
		} else if(currentCar.score > car2.score) {
		     car2 = currentCar;
		}
	    }
	    
	    this.population.populate(car1, car2);
	    this.currentStep = 0;
	    this.currentGeneration++;
	} else {
	    this.population.update();
	    this.findAllCarCollisions();
	    this.currentStep++;
	}
    }

    // Displays all the cars, goal, and obstacles in simulation
    show()
    {
	this.population.show();
	this.goal.show();
	for(var i = 0; i < this.obstacles.length; i++) {
	    this.obstacles[i].show();
	}
	this.goal.show();

	let percentDone = Math.floor((this.currentStep / this.lifetimeSteps) * 100);
	text("Current generation: " + this.currentGeneration + " (" + percentDone + "%)", 15, 15);
	text("Top score:     " + this.topScore, 15, 30);
	text("Top gen score: " + this.topGenerationScore, 15, 45);
    }
}
