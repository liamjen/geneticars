
class Population
{
    // carNum: number of cars in the population
    // startX: starting X of all cars
    // startY: starting Y of all cars
    constructor(carNum, startX, startY, lifetimeSteps, carSize, mutateChance)
    {
        this.cars = [];
        this.carNum = carNum;
	this.mutateChance = mutateChance;
        this.generation = 1;
        this.startX = startX;
        this.startY = startY;
	this.carSize = carSize;
	this.lifetimeSteps = lifetimeSteps;
	this.allDead = false; // True if all Cars have died

        for(var i = 0; i < this.carNum; i++)
        {
            this.cars[i] = new Car(startX, startY, lifetimeSteps, this.carSize, mutateChance);
        }
    }

    // Advances the simulation by 1 step (generally called each frame)
    update()
    {
	this.allDead = true;
	for(var i = 0; i < this.carNum; i++)
	{
	    let c = this.cars[i]; // Current car in loop
	    if(!c.dead) {
		this.allDead = false;
	        c.update();
	    } else {
		c.deadStep = c.step;
	    }
	
	}
    }

    // Populate next population based on 2 cars
    populate(car1, car2)
    {
	this.allDead = false;
        car1.dna.splice(car2.dna); // Combine DNA
        
	// Seed first two cars in population
        this.cars[0] = new Car(this.startX, this.startY, this.lifetimeSteps, this.carSize, this.mutateChance);
        this.cars[0].setDNA(car1.dna);
        this.cars[1] = new Car(this.startX, this.startY, this.lifetimeSteps, this.carSize, this.mutateChance);
        this.cars[1].setDNA(car2.dna);

	// Instantiate rest of population based on 2 cars
        for(var i = 2; i < this.carNum; i++)
        {
            this.cars[i] = new Car(this.startX, this.startY, this.lifetimeSteps, this.carSize, this.mutateChance);
	    // Flip coin to set to car1 or car2 dna (for child)
	    if(Math.random() > 0.5)
                this.cars[i].setDNA(car1.dna);
	    else
		this.cars[i].setDNA(car2.dna);

	    // Randomize some genes
            this.cars[i].dna.mutate();
        }
    }

    // Displays all the cars in the population
    show()
    {
        for(var i = 0; i < this.carNum; i++)
        {
            this.cars[i].show();
        }
    }
}
